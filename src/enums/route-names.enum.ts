export enum ROUTE_NAMES {
  app = 'app',
  posts = 'posts',
  postItem = 'post-item',
  postItemDeployment = 'post-item-deployment',
  postItemEditing = 'post-item-editing',
}
